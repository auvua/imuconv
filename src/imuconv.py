#!/usr/bin/env python

import numpy as np
import roslib
import rospy
import tf
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import Twist, Pose, Point, Quaternion, TransformStamped, PointStamped
from sensor_msgs.msg import Imu
from std_msgs.msg import Float64

from urdf_parser_py.urdf import URDF

depth = 0.0
robot = {}
depth_sensor_pt = (0,0,0)

def depth_callback(msg):
    global depth
    depth = msg.data


def imu_callback(msg):
    global depth
    global robot
    global depth_sensor_pt

    tb = tf2_ros.TransformBroadcaster()
    t = TransformStamped()
    qin = msg.orientation
    tf.transformations
    # rotate the IMU by -90deg about the x-axis to re-orient with robot frame
    adjusted_q = tf.transformations.quaternion_multiply([qin.x, qin.y, qin.z, qin.w], [-0.7071, 0, 0, 0.7071])
    orient = tf.transformations.euler_from_quaternion(adjusted_q)

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "world"
    t.child_frame_id = "base_link"
    q = tf.transformations.quaternion_from_euler(orient[0], -orient[1], -orient[2])
    t.transform.rotation.x = q[0]
    t.transform.rotation.y = q[1]
    t.transform.rotation.z = q[2]
    t.transform.rotation.w = q[3]
    t.transform.translation.x = 0.0
    t.transform.translation.y = 0.0
    t.transform.translation.z = 0.0

    p = PointStamped()
    p.header = t.header

    p.point = Point()
    p.point.x = depth_sensor_pt[0]
    p.point.y = depth_sensor_pt[1]
    p.point.z = depth_sensor_pt[2]

    
    transformed_point = tf2_geometry_msgs.do_transform_point(p, t)
    current_depth = transformed_point.point.z 
    adjusted_depth = -current_depth - depth

    t.transform.translation.z = adjusted_depth

    tb.sendTransform(t)

if __name__ == '__main__':
    global robot
    global depth_sensor_pt
    rospy.init_node('imuconv')
    robot = URDF.from_parameter_server("robot_description")

    for i in robot.joints:
        if i.name == 'body_2_ds':
            [x,y,z] = i.origin.xyz
            depth_sensor_pt = (x, y, z)
            break

    rospy.Subscriber('/imu', Imu, imu_callback)
    rospy.Subscriber('/depth', Float64, depth_callback)
    rospy.spin()
